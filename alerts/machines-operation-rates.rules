## Machine operations rate
ALERT RunnerMachineCreationRateHigh
  IF max(sum without(instance) (rate(ci_docker_machines{type="created"}[20m]))) > 5
  LABELS {channel="ci"}
  ANNOTATIONS {
    title="Machine creation rate for runners is too high: {{$value | printf \"%.2f\" }}",
    description="Machine creation rate for the last 20 minutes is over 5. This may by a symptom of problems with the auto-scaling provider. Check http://performance.gitlab.net/dashboard/db/ci.",
  }

## Shared runners machines creation rate
ALERT SharedRunnerMachineCreationRateLow
  IF max(sum without(instance) (rate(ci_docker_machines{type="created",job="shared-runners"}[10m]))) < 0.1
  LABELS {channel="ci"}
  ANNOTATIONS {
    title="Machine creation rate for shared runners is too low: {{$value | printf \"%.2f\" }}",
    description="Machine creation rate for shared runners for the last 10 minutes is less than 0.1. This may suggest problems with the auto-scaling provider. Check http://performance.gitlab.net/dashboard/db/ci.",
  }

## Shared runners machines usage rate
ALERT SharedRunnerMachineUsageRateLow
  IF max(sum without(instance) (rate(ci_docker_machines{type="used",job="shared-runners"}[10m]))) < 0.1
  LABELS {channel="ci"}
  ANNOTATIONS {
    title="Machine usage rate for shared runners is too low: {{$value | printf \"%.2f\" }}",
    description="Machine usage rate for shared runners for the last 10 minutes is less than 0.1. This may suggest problems with the auto-scaling provider. Check http://performance.gitlab.net/dashboard/db/ci.",
  }
